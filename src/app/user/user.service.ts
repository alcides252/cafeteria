import { UserModel } from './../model/user.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  url = "http://localhost:8096/cafeteria";
  constructor(private http: HttpClient) { }

  public getUsers(): Observable<UserModel>{
  	return this.http.get<UserModel>(this.url+'/marcas');
  }

  public delete(user: UserModel): Observable<UserModel>{
  	const urld = this.url + '/marcadelete/'+user.id_marca.toString();
  	return this.http.delete<UserModel>(urld);
  }


}
