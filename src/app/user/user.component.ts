import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserModel } from './../model/user.model';
import { UserService } from './user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  providers: [UserService]
})
export class UserComponent implements OnInit {

  private message: string = "";
  private deleted: boolean = false;
  private modified: boolean = false;
  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
  	if(sessionStorage.getItem('succes')){
		this.modified = true;
		this.message = sessionStorage.getItem('succes');
		sessionStorage.removeItem('succes')
  	}
  	this.loadUsers();
  }

  private loadUsers(): void {
  	this.userService.getUsers().subscribe(res =>{
  		this.users = res;
  	});

  public edit(user: UserModel): void {
  	sessionStorage.setItem('user',JSON.stringify(user));
  	this.router.navigate(['/createUserComponent']);
  	sessionStorage.setItem('succes',"Marca modificada exitosamente");
  }

  public delete(user: UserModel): void {
  	this.userService.delete(user).subscribe(res=>{
  		if(res==1){
  			this.loadUsers();
  			this.message = "Marca eliminada exitosamente";
  			this.deleted = true;
  		}
  	});
  }

  }
}
