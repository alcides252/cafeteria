import { Component, OnInit } from '@angular/core';

import { UserModel } from './../model/user.model';
import { CreateUserService } from './create-user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css'],
  providers: [CreateUserService]
})
export class CreateUserComponent implements OnInit {

  private user: UserModel;
  private isValid: boolean = true;
  private message: string = "";

  constructor(private createUserService: CreateUserService, private router: Router) { 
  	if(sessionStorage.getItem("user")){
  		this.user = JSON.parse(sessionStorage.getItem("user"));
  	}else{
  		this.user = new UserModel();
  	}
  }

  ngOnInit() {
  }

  public saveOrUpdate(): void {
  	if(!sessionStorage.getItem("user")){
	  	this.isValid = this.createUserService.validate(this.user);
	  	if(this.isValid){
	  		this.createUserService.save(this.user).subscribe(res =>{
		  		if(res==1){
		  			this.router.navigate(['/userComponent'])
  					sessionStorage.setItem('succes',"Marca creada exitosamente");
	  				//console.log("registradp");
		  		}else{
		  			console.log("Error");
		  		}
	  		});
	  	}else{
	  		this.message="Los campos con * son obligatorios";
	  	}
	}else{
  		this.createUserService.update(this.user).subscribe(res =>{
	  		if(res==1){
	  			this.router.navigate(['/userComponent'])
	  			//console.log("Modificado");
	  			sessionStorage.removeItem("user");
	  		}else{
	  			console.log("Error");
	  		}
  		});
	}
  }

}
