import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { RestResponse } from './../model/restResponse.model';
import { UserModel } from './../model/user.model';

@Injectable({
  providedIn: 'root'
})
export class CreateUserService {

  url = "http://localhost:8096/cafeteria";
  
  constructor(private http: HttpClient) {

  }

  public validate(user: UserModel):boolean{
  	let isValid = true;

    if(!user.nomb_marca){
      isValid = false;
    }
    if(!user.estado){
      isValid = false;
    }
  	return isValid;
  }

  public save(user: UserModel): Observable<RestResponse>{
    return this.http.post<RestResponse>(this.url+'/marcapost',user);
  }
  public update(user: UserModel): void{
    return this.http.put<RestResponse>(this.url+'/marcaput',user);
  }
  }
}
